/*
 * Copyright (C) 2019-2022 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */
#pragma once

#include "phog-wayland.h"
#include "monitor.h"

#include <gdk/gdk.h>
#include <glib-object.h>
#include <glib/gi18n.h>

G_BEGIN_DECLS

#define PHOG_TYPE_HEAD                 (phog_head_get_type ())

G_DECLARE_FINAL_TYPE (PhogHead, phog_head, PHOG, HEAD, GObject)

typedef struct _PhogHead PhogHead;

typedef struct _PhogHeadMode {
  /*< private >*/
  struct zwlr_output_mode_v1 *wlr_mode;
  PhogHead                  *head;

  int32_t                     width, height;
  int32_t                     refresh;
  gboolean                    preferred;
  char                       *name;
} PhogHeadMode;

struct _PhogHead {
  GObject                     parent;

  gchar                      *name;
  gchar                      *description;
  gchar                      *vendor, *product, *serial;
  gboolean                    enabled;

  struct {
    int32_t width, height;
  } phys;
  int32_t                     x, y;

  enum wl_output_transform    transform;
  double                      scale;
  PhogHeadMode              *mode;
  GPtrArray                  *modes;

  struct pending {
    int32_t x, y;
    enum wl_output_transform transform;
    PhogHeadMode *mode;
    double scale;
    gboolean enabled;
    gboolean seen;
  } pending;

  PhogMonitorConnectorType conn_type;
  struct zwlr_output_head_v1 *wlr_head;
};


PhogHead                  *phog_head_new_from_wlr_head (gpointer wlr_head);
struct zwlr_output_head_v1 *phog_head_get_wlr_head (PhogHead *self);
gboolean                    phog_head_get_enabled (PhogHead *self);
void                        phog_head_set_pending_enabled (PhogHead *self, gboolean enabled);
PhogHeadMode              *phog_head_get_preferred_mode (PhogHead *self);
gboolean                    phog_head_is_builtin (PhogHead *self);
PhogHeadMode              *phog_head_find_mode_by_name (PhogHead *self, const char *name);
float *                     phog_head_calculate_supported_mode_scales (PhogHead     *head,
                                                                        PhogHeadMode *mode,
                                                                        int           *n,
                                                                        gboolean       fractional);
void                        phog_head_clear_pending (PhogHead *self);
void                        phog_head_set_pending_transform (PhogHead             *self,
                                                              PhogMonitorTransform  transform,
                                                              GPtrArray             *heads);

G_END_DECLS
