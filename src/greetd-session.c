/*
 * Copyright (C) 2022 Collabora Ltd
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Arnaud Ferraris <arnaud.ferraris@collabora.com>
 */

#define G_LOG_DOMAIN "phog-greetd-session"

#include "phog-config.h"

#include "greetd-session.h"

#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <gio/gio.h>
#include <gio/gunixsocketaddress.h>

/**
 * SECTION:lockscreen
 * @short_description: The main lock screen
 * @Title: PhogGreetdSession
 *
 * The lock screen featuring the clock
 * and unlock keypad.
 */

typedef struct _PhogGreetdSession
{
    GObject parent;

    gchar *id;
    gchar *name;
    gchar *command;
    gchar *desktop_names;
} PhogGreetdSession;

G_DEFINE_TYPE (PhogGreetdSession, phog_greetd_session, G_TYPE_OBJECT)

static void
phog_greetd_session_constructed (GObject *object)
{
}


static void
phog_greetd_session_dispose (GObject *object)
{
    PhogGreetdSession *self = PHOG_GREETD_SESSION (object);

    g_clear_pointer (&self->name, g_free);
    g_clear_pointer (&self->command, g_free);
    g_clear_pointer (&self->desktop_names, g_free);

    G_OBJECT_CLASS (phog_greetd_session_parent_class)->dispose (object);
}


static void
phog_greetd_session_class_init (PhogGreetdSessionClass *klass)
{
  GObjectClass *object_class = (GObjectClass *)klass;

  object_class->constructed = phog_greetd_session_constructed;
  object_class->dispose = phog_greetd_session_dispose;
}


static void
phog_greetd_session_init (PhogGreetdSession *self)
{
}


GObject *
phog_greetd_session_new (void)
{
    return g_object_new (PHOG_TYPE_GREETD_SESSION, NULL);
}

const gchar *
phog_greetd_session_get_id (PhogGreetdSession *session)
{
    g_return_val_if_fail (PHOG_IS_GREETD_SESSION (session), NULL);
    return session->id;
}

const gchar *
phog_greetd_session_get_name (PhogGreetdSession *session)
{
    g_return_val_if_fail (PHOG_IS_GREETD_SESSION (session), NULL);
    return session->name;
}

const gchar *
phog_greetd_session_get_command (PhogGreetdSession *session)
{
    g_return_val_if_fail (PHOG_IS_GREETD_SESSION (session), NULL);
    return session->command;
}

const gchar *
phog_greetd_session_get_desktop_names (PhogGreetdSession *session)
{
    g_return_val_if_fail (PHOG_IS_GREETD_SESSION (session), NULL);
    return session->desktop_names;
}

void
phog_greetd_session_set_id (PhogGreetdSession *session, const gchar *id)
{
    g_return_if_fail (PHOG_IS_GREETD_SESSION (session));

    g_free (session->id);
    session->id = g_strdup (id);
}

void
phog_greetd_session_set_name (PhogGreetdSession *session, const gchar *name)
{
    g_return_if_fail (PHOG_IS_GREETD_SESSION (session));

    g_free (session->name);
    session->name = g_strdup (name);
}

void
phog_greetd_session_set_command (PhogGreetdSession *session, const gchar *command)
{
    g_return_if_fail (PHOG_IS_GREETD_SESSION (session));

    g_free (session->command);
    session->command = g_strdup (command);
}

void
phog_greetd_session_set_desktop_names (PhogGreetdSession *session, const gchar *desktop_names)
{
    g_return_if_fail (PHOG_IS_GREETD_SESSION (session));

    g_free (session->desktop_names);
    session->desktop_names = g_strdup (desktop_names);
}
