/*
 * Copyright (C) 2020 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#pragma once

#include <glib-object.h>
#include "phog-wwan-iface.h"
#include "wwan-manager.h"

G_BEGIN_DECLS

#define PHOG_TYPE_WWAN_OFONO (phog_wwan_ofono_get_type())

G_DECLARE_FINAL_TYPE (PhogWWanOfono, phog_wwan_ofono, PHOG, WWAN_OFONO, PhogWWanManager)

PhogWWanOfono *phog_wwan_ofono_new (void);

G_END_DECLS
