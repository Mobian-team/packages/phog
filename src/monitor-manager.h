/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */
#pragma once

#include "phog-display-dbus.h"
#include "monitor/monitor.h"

// #include "sensor-proxy-manager.h"

#include <glib-object.h>

G_BEGIN_DECLS

/**
 * PhogMonitorManagerConfigMethod:
 * @PHOG_MONITOR_MANAGER_CONFIG_METHOD_VERIFY: verify the configuration
 * @PHOG_MONITOR_MANAGER_CONFIG_METHOD_TEMPORARY: configuration is temporary
 * @PHOG_MONITOR_MANAGER_CONFIG_METHOD_PERSISTENT: configuration is permanent
 *
 * Equivalent to the 'method' enum in org.gnome.Mutter.DisplayConfig
 */
typedef enum _MetaMonitorsConfigMethod
{
  PHOG_MONITOR_MANAGER_CONFIG_METHOD_VERIFY = 0,
  PHOG_MONITOR_MANAGER_CONFIG_METHOD_TEMPORARY = 1,
  PHOG_MONITOR_MANAGER_CONFIG_METHOD_PERSISTENT = 2,
} PhogMonitorManagerConfigMethod;

#define PHOG_TYPE_MONITOR_MANAGER                 (phog_monitor_manager_get_type ())
G_DECLARE_FINAL_TYPE (PhogMonitorManager, phog_monitor_manager, PHOG, MONITOR_MANAGER,
                      PhogDBusDisplayConfigSkeleton)

PhogMonitorManager * phog_monitor_manager_new                       (void);
PhogMonitor        * phog_monitor_manager_get_monitor               (PhogMonitorManager *self,
                                                                       guint                num);
guint                 phog_monitor_manager_get_num_monitors          (PhogMonitorManager *self);
PhogMonitor        * phog_monitor_manager_find_monitor              (PhogMonitorManager *self,
                                                                       const char          *name);
void                  phog_monitor_manager_set_monitor_transform     (PhogMonitorManager *self,
                                                                       PhogMonitor        *monitor,
                                                                       PhogMonitorTransform transform);
void                  phog_monitor_manager_apply_monitor_config      (PhogMonitorManager *self);
gboolean              phog_monitor_manager_enable_fallback           (PhogMonitorManager *self);
void                  phog_monitor_manager_set_power_save_mode       (PhogMonitorManager *self,
                                                                       PhogMonitorPowerSaveMode mode);

G_END_DECLS
