/*
 * Copyright (C) 2018-2022 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 *
 * Somewhat based on maynard's panel which is
 * Copyright (C) 2014 Collabora Ltd. *
 * Author: Jonny Lamb <jonny.lamb@collabora.co.uk>
 */

#define G_LOG_DOMAIN "phog-top-panel"

#include "phog-config.h"

#include "shell.h"
#include "settings.h"
#include "top-panel.h"
#include "arrow.h"
#include "util.h"

#define GNOME_DESKTOP_USE_UNSTABLE_API
#include <libgnome-desktop/gnome-wall-clock.h>
#include <libgnome-desktop/gnome-xkb-info.h>

#include <glib/gi18n.h>

#define KEYBINDINGS_SCHEMA_ID "org.gnome.shell.keybindings"
#define KEYBINDING_KEY_TOGGLE_MESSAGE_TRAY "toggle-message-tray"

#define PHOG_TOP_PANEL_DRAG_THRESHOLD 0.3

/**
 * SECTION:top-panel
 * @short_description: The top panel
 * @Title: PhogTopPanel
 *
 * The top panel containing the top-bar (clock and status indicators) and, when activated
 * unfolds the #PhogSettings.
 */

enum {
  ACTIVATED,
  N_SIGNALS
};
static guint signals[N_SIGNALS] = { 0 };

typedef struct _PhogTopPanel {
  PhogDragSurface parent;

  PhogTopPanelState state;
  gboolean           on_lockscreen;

  /* Top row above settings */
  GtkWidget *btn_power;
  GtkWidget *menu_power;
  GtkWidget *lbl_clock2;
  GtkWidget *lbl_date;

  GtkWidget *stack;
  GtkWidget *arrow;
  GtkWidget *box;            /* main content box */
  GtkWidget *lbl_clock;      /* top-bar clock */
  GtkWidget *settings;       /* settings menu */
  GtkWidget *batteryinfo;

  GnomeWallClock *wall_clock;
  GnomeWallClock *wall_clock2;
  GdkSeat *seat;

  GSimpleActionGroup *actions;

  /* Keybinding */
  GStrv           action_names;
  GSettings      *kb_settings;

  GtkGesture     *click_gesture; /* needed so that the gesture isn't destroyed immediately */
} PhogTopPanel;

G_DEFINE_TYPE (PhogTopPanel, phog_top_panel, PHOG_TYPE_DRAG_SURFACE)


static void
update_drag_handle (PhogTopPanel *self, gboolean commit)
{
  gboolean success;
  gint handle;

  success = gtk_widget_translate_coordinates (GTK_WIDGET (self->settings),
                                              GTK_WIDGET (self),
                                              0, 0, NULL, &handle);
  if (!success)
    return;

  handle += phog_settings_get_drag_handle_offset (PHOG_SETTINGS (self->settings));

  g_debug ("Drag Handle: %d", handle);
  phog_drag_surface_set_drag_mode (PHOG_DRAG_SURFACE (self),
                                    PHOG_DRAG_SURFACE_DRAG_MODE_HANDLE);
  phog_drag_surface_set_drag_handle (PHOG_DRAG_SURFACE (self), handle);
  if (commit)
    phog_layer_surface_wl_surface_commit (PHOG_LAYER_SURFACE (self));
}


static void
on_settings_drag_handle_offset_changed (PhogTopPanel *self, GParamSpec   *pspec)
{
  update_drag_handle (self, TRUE);
}


static void
on_shutdown_action (GSimpleAction *action,
                    GVariant      *parameter,
                    gpointer       data)
{
  PhogTopPanel *self = PHOG_TOP_PANEL(data);
  // PhogSessionManager *sm = phog_shell_get_session_manager (phog_shell_get_default ());

  g_return_if_fail (PHOG_IS_TOP_PANEL (self));
  // phog_session_manager_shutdown (sm);
  phog_top_panel_fold (self);
}


static void
on_restart_action (GSimpleAction *action,
                   GVariant      *parameter,
                   gpointer       data)
{
  PhogTopPanel *self = PHOG_TOP_PANEL(data);
  // PhogSessionManager *sm = phog_shell_get_session_manager (phog_shell_get_default ());

  g_return_if_fail (PHOG_IS_TOP_PANEL (self));
  // g_return_if_fail (PHOG_IS_SESSION_MANAGER (sm));

  // phog_session_manager_reboot (sm);
  phog_top_panel_fold (self);
}


static void
wall_clock2_notify_cb (PhogTopPanel  *self,
                       GParamSpec     *pspec,
                       GnomeWallClock *wall_clock)
{
  const char *str;
  g_autofree char *date = NULL;

  g_return_if_fail (PHOG_IS_TOP_PANEL (self));
  g_return_if_fail (GNOME_IS_WALL_CLOCK (wall_clock));

  str = gnome_wall_clock_get_clock (wall_clock);
  gtk_label_set_text (GTK_LABEL (self->lbl_clock2), str);

  date = phog_util_local_date ();
  gtk_label_set_label (GTK_LABEL (self->lbl_date), date);
}


static gboolean
on_key_press_event (PhogTopPanel *self, GdkEventKey *event, gpointer data)
{
  gboolean handled = FALSE;

  g_return_val_if_fail (PHOG_IS_TOP_PANEL (self), FALSE);

  if (!self->settings)
    return handled;

  switch (event->keyval) {
    case GDK_KEY_Escape:
      phog_top_panel_fold (self);
      handled = TRUE;
      break;
    default:
      /* nothing to do */
      break;
    }
  return handled;
}


static void
released_cb (PhogTopPanel *self, int n_press, double x, double y, GtkGestureMultiPress *gesture)
{
  /*
   * The popover has to be popdown manually as it doesn't happen
   * automatically when the power button is tapped with touch
   */
  if (gtk_widget_is_visible (self->menu_power)) {
    gtk_popover_popdown (GTK_POPOVER (self->menu_power));
    return;
  }

  if (phog_util_gesture_is_touch (GTK_GESTURE_SINGLE (gesture)) == FALSE)
    g_signal_emit (self, signals[ACTIVATED], 0);
}


static void
toggle_message_tray_action (GSimpleAction *action, GVariant *param, gpointer data)
{
  PhogTopPanel *self = PHOG_TOP_PANEL (data);

  g_return_if_fail (PHOG_IS_TOP_PANEL (self));

  phog_top_panel_toggle_fold (self);
  /* TODO: focus message tray */
}


static void
add_keybindings (PhogTopPanel *self)
{
  g_auto (GStrv) keybindings = NULL;
  g_autoptr (GArray) actions = g_array_new (FALSE, TRUE, sizeof (GActionEntry));

  keybindings = g_settings_get_strv (self->kb_settings, KEYBINDING_KEY_TOGGLE_MESSAGE_TRAY);
  for (int i = 0; i < g_strv_length (keybindings); i++) {
    GActionEntry entry = { .name = keybindings[i], .activate = toggle_message_tray_action };
    g_array_append_val (actions, entry);
  }
  phog_shell_add_global_keyboard_action_entries (phog_shell_get_default (),
                                                  (GActionEntry*) actions->data,
                                                  actions->len,
                                                  self);
  self->action_names = g_steal_pointer (&keybindings);
}


static void
on_keybindings_changed (PhogTopPanel *self,
                        gchar         *key,
                        GSettings     *settings)
{
  /* For now just redo all keybindings */
  g_debug ("Updating keybindings");
  phog_shell_remove_global_keyboard_action_entries (phog_shell_get_default (),
                                                     self->action_names);
  g_clear_pointer (&self->action_names, g_strfreev);
  add_keybindings (self);
}


static void
phog_top_panel_dragged (PhogDragSurface *self, int margin)
{
  PhogTopPanel *panel = PHOG_TOP_PANEL (self);
  int width, height;
  gtk_window_get_size (GTK_WINDOW (self), &width, &height);
  phog_arrow_set_progress (PHOG_ARROW (panel->arrow), -margin / (double)(height - PHOG_TOP_PANEL_HEIGHT));
  g_debug ("Margin: %d", margin);
}


static void
on_drag_state_changed (PhogTopPanel *self)
{
  PhogTopPanelState state = self->state;
  const char *visible;
  gboolean kbd_interactivity = FALSE;
  double arrow = -1.0;

  /* Close the popover on any drag */
  gtk_widget_hide (self->menu_power);

  switch (phog_drag_surface_get_drag_state (PHOG_DRAG_SURFACE (self))) {
  case PHOG_DRAG_SURFACE_STATE_UNFOLDED:
    state = PHOG_TOP_PANEL_STATE_UNFOLDED;
    update_drag_handle (self, TRUE);
    kbd_interactivity = TRUE;
    visible = "arrow";
    arrow = 0.0;
    break;
  case PHOG_DRAG_SURFACE_STATE_FOLDED:
    state = PHOG_TOP_PANEL_STATE_FOLDED;
    visible = "top-bar";
    arrow = 1.0;
    break;
  case PHOG_DRAG_SURFACE_STATE_DRAGGED:
    visible = "arrow";
    arrow = phog_arrow_get_progress (PHOG_ARROW (self->arrow));
    break;
  default:
    g_return_if_reached ();
  }

  g_debug ("%s: state: %d, visible: %s", __func__, self->state, visible);
  gtk_stack_set_visible_child_name (GTK_STACK (self->stack), visible);
  phog_arrow_set_progress (PHOG_ARROW (self->arrow), arrow);

  self->state = state;
  phog_layer_surface_set_kbd_interactivity (PHOG_LAYER_SURFACE (self), kbd_interactivity);
  phog_layer_surface_wl_surface_commit (PHOG_LAYER_SURFACE (self));
}


static GActionEntry entries[] = {
  { .name = "poweroff", .activate = on_shutdown_action },
  { .name = "restart", .activate = on_restart_action },
};


static void
phog_top_panel_constructed (GObject *object)
{
  PhogTopPanel *self = PHOG_TOP_PANEL (object);

  G_OBJECT_CLASS (phog_top_panel_parent_class)->constructed (object);

  self->state = PHOG_TOP_PANEL_STATE_FOLDED;

  self->wall_clock = gnome_wall_clock_new ();
  g_object_set (self->wall_clock, "time-only", TRUE, NULL);
  g_object_bind_property (self->wall_clock, "clock",
                          self->lbl_clock, "label",
                          G_BINDING_SYNC_CREATE);

  self->wall_clock2 = gnome_wall_clock_new ();
  g_object_set (self->wall_clock2, "time-only", TRUE, NULL);
  g_signal_connect_object (self->wall_clock2,
                           "notify::clock",
                           G_CALLBACK (wall_clock2_notify_cb),
                           self,
                           G_CONNECT_SWAPPED);
  wall_clock2_notify_cb (self, NULL, self->wall_clock2);

  gtk_window_set_title (GTK_WINDOW (self), "phog panel");

  /* Settings menu and it's top-panel / menu */
  gtk_widget_add_events (GTK_WIDGET (self), GDK_ALL_EVENTS_MASK);
  g_signal_connect (G_OBJECT (self),
                    "key-press-event",
                    G_CALLBACK (on_key_press_event),
                    NULL);
  g_signal_connect_swapped (self->settings,
                            "setting-done",
                            G_CALLBACK (phog_top_panel_fold),
                            self);

  self->actions = g_simple_action_group_new ();
  gtk_widget_insert_action_group (GTK_WIDGET (self), "panel",
                                  G_ACTION_GROUP (self->actions));
  g_action_map_add_action_entries (G_ACTION_MAP (self->actions),
                                   entries, G_N_ELEMENTS (entries),
                                   self);
  if (!phog_shell_started_by_display_manager (phog_shell_get_default ())) {
    GAction *action = g_action_map_lookup_action (G_ACTION_MAP (self->actions),
                                                  "logout");
    g_simple_action_set_enabled (G_SIMPLE_ACTION(action), FALSE);
  }

  g_signal_connect_swapped (self->kb_settings,
                            "changed::" KEYBINDING_KEY_TOGGLE_MESSAGE_TRAY,
                            G_CALLBACK (on_keybindings_changed),
                            self);
  add_keybindings (self);

  g_signal_connect (self, "notify::drag-state", G_CALLBACK (on_drag_state_changed), NULL);
}


static void
phog_top_panel_dispose (GObject *object)
{
  PhogTopPanel *self = PHOG_TOP_PANEL (object);

  g_clear_object (&self->kb_settings);
  g_clear_object (&self->wall_clock);
  g_clear_object (&self->wall_clock2);
  g_clear_object (&self->actions);
  g_clear_pointer (&self->action_names, g_strfreev);
  if (self->seat) {
    /* language indicator */
    g_signal_handlers_disconnect_by_data (self->seat, self);
    self->seat = NULL;
  }

  G_OBJECT_CLASS (phog_top_panel_parent_class)->dispose (object);
}


static int
get_margin (gint height)
{
  return (-1 * height) + PHOG_TOP_PANEL_HEIGHT;
}


static gboolean
on_configure_event (PhogTopPanel *self, GdkEventConfigure *event)
{
  guint margin;

  margin = get_margin (event->height);

  /* ignore popovers like the power menu */
  if (gtk_widget_get_window (GTK_WIDGET (self)) != event->window)
    return FALSE;

  g_debug ("%s: %dx%d margin: %d", __func__, event->height, event->width, margin);

  /* If the size changes we need to update the folded margin */
  phog_drag_surface_set_margin (PHOG_DRAG_SURFACE (self), margin, 0);
  /* Update drag handle since top-panel size might have changed */
  update_drag_handle (self, FALSE);
  phog_layer_surface_wl_surface_commit (PHOG_LAYER_SURFACE (self));

  return FALSE;
}


static void
phog_top_panel_configured (PhogLayerSurface *layer_surface)
{
  guint width, height;

  width = phog_layer_surface_get_configured_width  (layer_surface);
  height = phog_layer_surface_get_configured_height (layer_surface);

  g_debug ("%s: %dx%d", __func__, width, height);

  PHOG_LAYER_SURFACE_CLASS (phog_top_panel_parent_class)->configured (layer_surface);
}


static void
phog_top_panel_class_init (PhogTopPanelClass *klass)
{
  GObjectClass *object_class = (GObjectClass *)klass;
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  PhogLayerSurfaceClass *layer_surface_class = PHOG_LAYER_SURFACE_CLASS (klass);
  PhogDragSurfaceClass *drag_surface_class = PHOG_DRAG_SURFACE_CLASS (klass);

  object_class->constructed = phog_top_panel_constructed;
  object_class->dispose = phog_top_panel_dispose;

  layer_surface_class->configured = phog_top_panel_configured;

  drag_surface_class->dragged = phog_top_panel_dragged;

  gtk_widget_class_set_css_name (widget_class, "phog-top-panel");

  signals[ACTIVATED] = g_signal_new ("activated",
      G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST, 0, NULL, NULL,
      NULL, G_TYPE_NONE, 0);

  g_type_ensure (PHOG_TYPE_ARROW);

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/mobian/phog/ui/top-panel.ui");
  gtk_widget_class_bind_template_child (widget_class, PhogTopPanel, arrow);
  gtk_widget_class_bind_template_child (widget_class, PhogTopPanel, menu_power);
  gtk_widget_class_bind_template_child (widget_class, PhogTopPanel, btn_power);
  gtk_widget_class_bind_template_child (widget_class, PhogTopPanel, batteryinfo);
  gtk_widget_class_bind_template_child (widget_class, PhogTopPanel, lbl_clock);
  gtk_widget_class_bind_template_child (widget_class, PhogTopPanel, lbl_clock2);
  gtk_widget_class_bind_template_child (widget_class, PhogTopPanel, lbl_date);
  gtk_widget_class_bind_template_child (widget_class, PhogTopPanel, box);
  gtk_widget_class_bind_template_child (widget_class, PhogTopPanel, stack);
  gtk_widget_class_bind_template_child (widget_class, PhogTopPanel, settings);
  gtk_widget_class_bind_template_child (widget_class, PhogTopPanel, click_gesture);
  gtk_widget_class_bind_template_callback (widget_class, on_settings_drag_handle_offset_changed);
  gtk_widget_class_bind_template_callback (widget_class, released_cb);
}


static void
phog_top_panel_init (PhogTopPanel *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->state = PHOG_TOP_PANEL_STATE_FOLDED;
  self->kb_settings = g_settings_new (KEYBINDINGS_SCHEMA_ID);
  g_signal_connect (self, "configure-event", G_CALLBACK (on_configure_event), NULL);
}


GtkWidget *
phog_top_panel_new (struct zwlr_layer_shell_v1          *layer_shell,
                     struct zphoc_layer_shell_effects_v1 *layer_shell_effects,
                     struct wl_output                    *wl_output,
                     guint32                              layer,
                     int                                  height)
{
  return g_object_new (PHOG_TYPE_TOP_PANEL,
                       /* layer-surface */
                       "layer-shell", layer_shell,
                       "wl-output", wl_output,
                       "height", height,
                       "anchor", ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP |
                                 ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT |
                                 ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT,
                       "layer", layer,
                       "kbd-interactivity", FALSE,
                       "namespace", "phog top-panel",
                       /* drag-surface */
                       "layer-shell-effects", layer_shell_effects,
                       "exclusive", PHOG_TOP_PANEL_HEIGHT,
                       "threshold", PHOG_TOP_PANEL_DRAG_THRESHOLD,
                       NULL);
}


void
phog_top_panel_fold (PhogTopPanel *self)
{
  g_return_if_fail (PHOG_IS_TOP_PANEL (self));

  if (self->state == PHOG_TOP_PANEL_STATE_FOLDED)
	return;

  phog_drag_surface_set_drag_state (PHOG_DRAG_SURFACE (self),
                                     PHOG_DRAG_SURFACE_STATE_FOLDED);
}


void
phog_top_panel_unfold (PhogTopPanel *self)
{
  g_return_if_fail (PHOG_IS_TOP_PANEL (self));

  if (self->state == PHOG_TOP_PANEL_STATE_UNFOLDED)
	return;

  phog_drag_surface_set_drag_state (PHOG_DRAG_SURFACE (self),
                                     PHOG_DRAG_SURFACE_STATE_UNFOLDED);
}


void
phog_top_panel_toggle_fold (PhogTopPanel *self)
{
  g_return_if_fail (PHOG_IS_TOP_PANEL (self));

  if (self->state == PHOG_TOP_PANEL_STATE_UNFOLDED) {
    phog_top_panel_fold (self);
  } else {
    phog_top_panel_unfold (self);
  }
}


PhogTopPanelState
phog_top_panel_get_state (PhogTopPanel *self)
{
  g_return_val_if_fail (PHOG_IS_TOP_PANEL (self), PHOG_TOP_PANEL_STATE_FOLDED);

  return self->state;
}
