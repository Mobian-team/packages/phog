/*
 * Copyright (C) 2022 Collabora Ltd
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Arnaud Ferraris <arnaud.ferraris@collabora.com>
 */

#pragma once

#include <glib.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define PHOG_TYPE_GREETD_SESSION (phog_greetd_session_get_type ())

G_DECLARE_FINAL_TYPE (PhogGreetdSession, phog_greetd_session, PHOG, GREETD_SESSION,
                      GObject)

GObject     *phog_greetd_session_new (void);

const gchar *phog_greetd_session_get_id (PhogGreetdSession *session);
const gchar *phog_greetd_session_get_name (PhogGreetdSession *session);
const gchar *phog_greetd_session_get_command (PhogGreetdSession *session);
const gchar *phog_greetd_session_get_desktop_names (PhogGreetdSession *session);

void         phog_greetd_session_set_id (PhogGreetdSession *session, const gchar *id);
void         phog_greetd_session_set_name (PhogGreetdSession *session, const gchar *name);
void         phog_greetd_session_set_command (PhogGreetdSession *session, const gchar *command);
void         phog_greetd_session_set_desktop_names (PhogGreetdSession *session, const gchar *desktop_names);

G_END_DECLS
