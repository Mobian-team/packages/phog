# Getting started with Phog development

## Overview
Phog is a graphical, mobile-friendly greeter for
[greetd](https://git.sr.ht/~kennylevinsen/greetd). It targets mobile devices
with small screens like Purism's Librem 5 and Pine64's PinePhone.

Its purpose is to provide a graphical interface for users to enter their login
credentials and select the session they wish to launch.

Since it acts as a Wayland client it needs a compositor to function
that provides the necessary protocols (most notably
wlr-layer-shell). It has only been tested so far with
[phoc](https://gitlab.gnome.org/World/Phosh/phoc) (the PHOne Compositor).

It can also (optionally) interface with
[upower](https://gitlab.freedesktop.org/upower/upower) for battery state
reporting, [NetworkManager](https://www.networkmanager.dev/) for WiFi
monitoring and either
[ModemManager](https://www.freedesktop.org/wiki/Software/ModemManager/)
or [oFono](https://git.kernel.org/pub/scm/network/ofono/ofono.git) for
reporting the state of modem connectivity.

It uses [GTK](https://www.gtk.org/) as it's GUI toolkit and
[libhandy](https://gitlab.gnome.org/GNOME/libhandy) for adaptive
widgets.

Although targeted at touch devices Phog does not implement a
on screen keyboard (OSK) but leaves this to
[squeekboard](https://gitlab.gnome.org/World/Phosh/squeekboard).

Phog is therefore designed to integrate and work perfectly on devices running
the Phosh ecosystem.

### Wayland protocols
Since Phoc (in contrast to some other solutions) aims to be a minimal
Wayland compositor that manages rendering, handle physical and virtual
input and display devices but not much more it needs to provide some
more protocols to enable graphical shells like Phog. These are usually
from [wlr-protocols](https://github.com/swaywm/wlr-protocols) and
Phoc uses the [wlroots](https://github.com/swaywm/wlroots) library for
implementing them.

These are the most prominent ones use by Phog:

- wlr-layer-shell: Usually Wayland clients have little influence on where
  the compositor places them. This protocol gives Phog enough room
  to build the top bar via #PhogTopPanel and
  lock screens via #PhogLockscreen.
- wlr-foreign-toplevel-management: This allows the management of
  toplevels (windows).
- wlr-output-management: This allows to manage a monitors power
  state (to e.g.turn it off when unused).

Besides those Phog uses a number of "regular" Wayland client
protocols like `xdg_output`, `wl_output` or `wl_seat` (see
#PhogWayland for the full list).

### Session startup

Since Phog is a standalone Wayland application it needs to be started by a
compositor. The startup script must also start `squeekboard` in order to allow
entering complex (as opposed to numeric-only) passwords.

```
phoc (compositor) -> phog
                  -> squeekboard
```

## Hints
This is a unsorted list of hints when developing for Phog

### Manager Objects

Phog uses several manager objects e.g. #PhogMonitorManager,
#PhogLockscreenManager to keep track of certain objects (monitors, lock screens)
and to trigger events on those when needed.  They're usually created and
disposed by #PhogShell. Some of them like #PhogWayland are singletons so you can
access them from basically anywhere in the codebase.

### Status Information Widgets

Several widgets listen to changes on DBus objects in order to e.g. display the
current connectivity - see #PhogConnectivityInfo for an example that monitors
network connectivity.

Sometimes it is no longer useful to show the widget (since
e.g. the corresponding DBus service went away). In that case the
widget should flip a boolean property so the parent container can
hide the object via #g_object_bind_property().

### Debugging

Since phog is a GTK application you can use
[GtkInspector](https://wiki.gnome.org/Projects/GTK/Inspector).
You can use the `GTK_INSPECTOR_DISPLAY` enviroment variable to use a different
Wayland display for the inspector window. This can be useful to have the
inspector windows outside of a nested Wayland session.
