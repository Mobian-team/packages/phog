/*
 * Copyright (C) 2020 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */

#include "phog-wayland.h"
#include "monitor/monitor.h"

#include <gtk/gtk.h>

#include <linux/input-event-codes.h>

#pragma once

G_BEGIN_DECLS

typedef struct _PhogTestCompositorState {
  GPid                        pid;
  PhogWayland               *wl;
  struct wl_output           *output;
  GdkDisplay                 *gdk_display;
} PhogTestCompositorState;

PhogTestCompositorState *phog_test_compositor_new (gboolean heads_stub);
void                      phog_test_compositor_free (PhogTestCompositorState *state);
PhogMonitor*             phog_test_get_monitor(void);
struct zwp_virtual_keyboard_v1 * phog_test_keyboard_new (PhogWayland *wl);
void                      phog_test_keyboard_press_keys (struct zwp_virtual_keyboard_v1 *keyboard,
                                                          GTimer *timer, ...) G_GNUC_NULL_TERMINATED;
void                      phog_test_keyboard_press_modifiers   (struct zwp_virtual_keyboard_v1 *keyboard,
                                                                 guint                           modifiers);
void                      phog_test_keyboard_release_modifiers (struct zwp_virtual_keyboard_v1 *keyboard);

G_END_DECLS
