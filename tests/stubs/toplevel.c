/*
 * Copyright (C) 2019 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Sebastian Krzyszkowiak <sebastian.krzyszkowiak@puri.sm>
 */

#include "toplevel.h"

enum {
  SIGNAL_CLOSED,
  N_SIGNALS
};
static guint signals[N_SIGNALS] = { 0 };

struct _PhogToplevel {
  GObject parent;
};

G_DEFINE_TYPE (PhogToplevel, phog_toplevel, G_TYPE_OBJECT);

static void
phog_toplevel_class_init (PhogToplevelClass *klass)
{
  signals[SIGNAL_CLOSED] = g_signal_new (
    "closed",
    G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST, 0, NULL, NULL,
    NULL, G_TYPE_NONE, 0);
}


static void
phog_toplevel_init (PhogToplevel *self)
{
}


const char *
phog_toplevel_get_title (PhogToplevel *self) {
  g_return_val_if_fail (PHOG_IS_TOPLEVEL (self), NULL);
  return "Mock Title";
}


const char *
phog_toplevel_get_app_id (PhogToplevel *self) {
  g_return_val_if_fail (PHOG_IS_TOPLEVEL (self), NULL);
  return "mock.app.id";
}


gboolean
phog_toplevel_is_configured (PhogToplevel *self) {
  g_return_val_if_fail (PHOG_IS_TOPLEVEL (self), FALSE);
  return FALSE;
}


gboolean
phog_toplevel_is_activated (PhogToplevel *self) {
  g_return_val_if_fail (PHOG_IS_TOPLEVEL (self), FALSE);
  return FALSE;
}


gboolean
phog_toplevel_is_maximized (PhogToplevel *self)
{
  g_return_val_if_fail (PHOG_IS_TOPLEVEL (self), FALSE);
  return FALSE;
}


gboolean
phog_toplevel_is_fullscreen (PhogToplevel *self)
{
  g_return_val_if_fail (PHOG_IS_TOPLEVEL (self), FALSE);
  return FALSE;
}

void
phog_toplevel_activate (PhogToplevel *self, struct wl_seat *seat) {
  g_return_if_fail (PHOG_IS_TOPLEVEL (self));
}


void
phog_toplevel_close (PhogToplevel *self) {
  g_return_if_fail (PHOG_IS_TOPLEVEL (self));
}


PhogToplevel *
phog_toplevel_new_from_handle (struct zwlr_foreign_toplevel_handle_v1 *handle)
{
  return g_object_new (PHOG_TYPE_TOPLEVEL, "handle", handle, NULL);
}
